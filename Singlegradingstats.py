import urllib
from urllib.request import urlopen
import re
import string
import webbrowser
import numpy as np
import pylab as p
import Multigradestats
import gradecaller

dept=['AE','CL','CH','CE','CS','GS','EE','HS','MA','SI','ME','MM','PH','EP','GP']
gradelist=['AP','AA','AB','BB','BC','CC','CD','DD','FR','II']
year,sem,course="","",""


def singlegradingstats(str1):
	global year
	global sem
	global course
	#print ("Success")
	#print(str1)
	matchcourse=re.search(r'\b\w\w\d\d\d\b|\b\w\w\s\d\d\d\b', str1,re.I)
	matchyear=re.search(r'\b\d\d\d\d\b', str1,flags=0)
	str1=re.sub('Autumn','1',str1,re.I)
	str1=re.sub('Spring','2',str1,re.I)
	matchsem=re.search(r'\b1\b|\b2\b',str1,flags=0)
	
	if matchcourse:
		course=matchcourse.group()
		
	if matchyear:
		year=matchyear.group()
		
		
	if matchsem:
		sem=matchsem.group()
	
	gflag =0
	grade=""
	for g in gradelist:
		matchgrade=re.search(g,str1,re.I)
		if matchgrade:
			grade=matchgrade.group()
			gflag=1
			break
	#if(gflag==0):
		#grade=input("Enter the grade\n").upper()

	address='http://asc.iitb.ac.in/academic/Grading/statistics/gradstatforcrse.jsp?year='+year+'&semester='+sem+'&txtcrsecode='+course+'&submit=SUBMIT'
	#print(address)
	source=gradecaller.call(address)
	match = re.search(grade,source)
	match = re.compile(r'\d+').search(source, match.end())
	#print("Number of "+grade+"s are:"+ match.group())

	gradestats1=Multigradestats.allgradestats(address)

	print("Median grade:"+Multigradestats.median(gradestats1))
	print("Mean grade:"+'%.2f'%(Multigradestats.mean(gradestats1)))
	print(year,sem,course)
	Multigradestats.graph_1(gradestats1,year,sem,course)
	