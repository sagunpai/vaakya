import re
import urllib
from urllib.request import urlopen
import urllib.request as req
import urllib.request
import urllib.parse
import sys
from sys import argv
import Singlegradingstats
import Twocoursestats
import os

if os.path.exists('graph1.png'):
	os.remove("graph1.png")
if os.path.exists('graph2.png'):
	os.remove("graph2.png")
flag1=0
flag2=0
flag3=0

query=""
for i in range(1,len(sys.argv)):
	query=query+sys.argv[i]+" "
#query=input("Enter query: ")
query=query.strip()

#print (query)

courses = re.findall(r'\w\w\d\d\d|\w\w\s\d\d\d', query) 
for course in courses:
	flag1+=1

years = re.findall(r'\d\d\d\d', query) 
for year in years:
	flag2+=1

sems = re.findall(r' \d', query) 
for sem in sems:
	flag3+=1

flag3 = flag3-flag2
	
#print(flag1,flag2,flag3)
if (flag1==1 and flag2==1 and flag3==1):
	Singlegradingstats.singlegradingstats(query)
elif (flag1==2 and flag2==2 and flag3==2):
	Twocoursestats.twogradestats(query)
