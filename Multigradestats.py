import urllib
import re
import gradecaller
from urllib.request import urlopen
import matplotlib
matplotlib.use("Agg") 
import numpy as np
import pylab as p
from pylab import *
rcParams['figure.figsize'] = 10, 10




gradelist=['AP','AA','AB','BB','BC','CC','CD','DD','FR','II']

def totalgrades(gradestats):
	total=0
	for i in gradestats:
		total+=int(i)
	return(total)	
	
def allgradestats(address):
	#print(address)
	#address1= ('http://asc.iitb.ac.in/academic/Grading/statistics/gradstatforcrse.jsp?year='+str(input("which year?"))+'&semester='+str(input("which semester?"))+'&txtcrsecode='+str(input("Which course?"))+'&submit=SUBMIT')
	
	source=gradecaller.call(address)
	#print(source)

	def grade_number(str):
		grade=str.upper()
		
		if grade=='TOTAL':
			print(grade)
			match = re.findall(r'\d+', source)[-1]
			print(match)
			return match

		else:
			#print(grade)
			match = re.search(grade, source)
			if match:
				match = re.compile(r'\d+').search(source, match.end())
				#print(match.group())
				return match.group()
		 		
	year=re.search(r'\d\d\d\d',address).group()
	sem=re.search(r'semester=\d',address).group()[9]
	course=re.search(r"\b\w\w\s\d\d\d\b|\b\w\w\d\d\d\b",address).group()
	gradestats=[]
	print("Grades for the course "+course+" in semester "+sem+", "+year+" are:")
	for i in gradelist:
			gradestats.append(0 if (grade_number(i)==None) else grade_number(i))
	for i in range(10):
		print (gradelist[i],gradestats[i])
		print("\n")
	print("Total",totalgrades(gradestats))	
	print('\n\n')
	return(gradestats)

def graph_2(gradestats1,gradestats2,course1,course2,sem1,sem2,year1,year2):
	grades = [1,2,3,4,5,6,7,8,9,10]
	gradesw=[]
	number1 = []
	number2 = []
	labels =[]
	total1,total2=0,0
	count=0
	for i in gradestats1:
		x, y = i,gradestats1[count]
		if (
		y==None):
			y=0
		labels.append(x)
		number1.append(int(y))	
		total1+=int(y)
		count+=1
	count=0
	for i in gradestats2:
		y = gradestats2[count]
		if (y==None):
			y=0
		number2.append(int(y))
		total2+=int(y)
		count+=1
	width = 0.40
	for i in grades:
		gradesw.append(i+width)	
	#print(number1)
	#print(number2)
	#print(labels)
	fig = p.figure()
	
	#fig.set_size_inches(18.5,10.5)
	ax=fig.add_subplot(1,1,1)
	rect1 = ax.bar(grades,number1,width,color = 'r')
	rect2 = ax.bar(gradesw,number2,width,color = 'b')
	def autolabel(rects,total):
		# attach some text labels
		for rect in rects:
			height = rect.get_height()
			ax.text(rect.get_x()+rect.get_width()/2., 1.0005*height, '%d(%d%%)'%(int(height), 100*(int(height)/total)),ha='center', va='bottom',size='x-small')	
	ax.set_ylabel('Number of students')
	ax.set_xlabel("Grades")
	ax.set_title('Comparative bar plot for '+course1.upper()+",Sem "+sem1+','+year1 +" versus "+course2.upper()+",Sem "+sem2+','+year2)
	ax.set_xticks(gradesw)
	p.xticks(gradesw,gradelist,horizontalalignment='center')
	autolabel(rect1,total1)
	autolabel(rect2,total2)
	ax.legend([rect1,rect2],[course1.upper()+",Sem "+sem1+','+year1 ,course2.upper()+",Sem "+sem2+','+year2])
	p.savefig(".\graph2.png")
	#p.show()

	
	
def median(gradestats):
	total=totalgrades(gradestats)
	med=int(total/2)
	count=0
	for i in range(10):	
		count+=int(gradestats[i])
		if (count>med):
			return(gradelist[i])
			
def mean(gradestats):
	total=totalgrades(gradestats)
	totalg=int(gradestats[0])*10
	grd=10
	for i in range (1,8):
		totalg+=grd*int(gradestats[i])
		grd-=1
	return(totalg/total)	
		

def graph_1(gradestats1,year,sem,course):

	grades = [1,2,3,4,5,6,7,8,9,10]
	gradesw=[]
	number1 = []
	number2 = []
	labels =[]
	total1=0
	count=0
	for i in gradestats1:
		x, y = i,gradestats1[count]
		if (y==None):
			y=0
		labels.append(x)
		number1.append(int(y))	
		total1+=int(y)
		count+=1
	count=0
	width = 0.40
	#print(number1)
	#print(number2)
	#print(labels)
	fig = p.figure()
	ax=fig.add_subplot(1,1,1)
	rect1 = ax.bar(grades,number1,width,color = 'b')
	def autolabel(rects,total):
		# attach some text labels
		for rect in rects:
			height = rect.get_height()
			ax.text(rect.get_x()+rect.get_width()/2., 1.0005*height, '%d(%.2f%%)'%(int(height), 100*(int(height)/total)),ha='center', va='bottom',size='x-small')	
	ax.set_ylabel('Number of students')
	ax.set_xlabel("Grades")
	ax.set_title('Bar plot for grades for '+course.upper()+",Sem "+sem+','+year )
	ax.set_xticks(grades)
	p.xticks(grades,gradelist,horizontalalignment='center')
	autolabel(rect1,total1)
	ax.legend()
	p.savefig(".\graph1.png")
	#p.show()

def main():

	gradestats1=allgradestats('http://asc.iitb.ac.in/academic/Grading/statistics/gradstatforcrse.jsp?year='+str(input("which year?"))+'&semester='+str(input("which semester?"))+'&txtcrsecode='+str(input("Which course?"))+'&submit=SUBMIT')
	#print(gradestats1)
	gradestats2=allgradestats('http://asc.iitb.ac.in/academic/Grading/statistics/gradstatforcrse.jsp?year='+str(input("which year?"))+'&semester='+str(input("which semester?"))+'&txtcrsecode='+str(input("Which course?"))+'&submit=SUBMIT')
	#print(gradestats2)
	print("Median 1:",median(gradestats1),"Median 2:",median(gradestats2))
	print("Mean",'%.2f'%(mean(gradestats1)),"Mean 2:",'%.2f'%(mean(gradestats2)))	
		
	graph_1(gradestats1)
	graph_2(gradestats1,gradestats2,course1,course2)
#main()