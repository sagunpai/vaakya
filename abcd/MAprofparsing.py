import urllib
from urllib.request import urlopen
import re
import string
import webbrowser

def profinfo_MA(profquery):

	
	#print(profquery)
	
	address ="http://www.math.iitb.ac.in/people/faculty/faculty.html"
	source=urlopen(address).read()
	source=str(source)
	#print(" ".join(profquery.split(' ')[::-1]))
	from html.parser import HTMLParser
				
	class MyHTMLParser(HTMLParser):
		
		flag=0
		depflag=0
		
		#self.flag=0
		def handle_data(self, data):
			#print(self.flag)
			x1=str(data)
			x=x1.upper()
			#print (x)
			
			
			if(re.search(profquery,x,re.I) or re.search(" ".join(profquery.split(' ')[::-1]),x,re.I)):
				self.flag=1
				if self.depflag==0:
					self.depflag=1
					print("\n Mathematics:\n")
				
			if (self.flag>=1 and self.flag<=6):
				if (re.search(r"\\.",x1)):
					x1=re.sub(r"\\.","",x1)
				if (self.flag==4):
					print("Education:")
				if(self.flag==6):
					print("Research Interests:")	
				print(x1)
				self.flag+=1
				
	
				
	parser = MyHTMLParser(strict=False)
	parser.feed(source)
