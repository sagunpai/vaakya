import urllib
from urllib.request import urlopen
import re
import string
import webbrowser
import numpy as np
import pylab as p
import Multigradestats
import sys
import Slowprofparsing

from sys import argv
query=""
for i in range(1,len(sys.argv)):
	query=query+sys.argv[i]+" "
query=query.strip()
#query=input()

dept=['AE','CL','CH','CE','CS','GS','EE','HS','MA','SI','ME','MM','PH','EP','GP']
gradelist=['AP','AA','AB','BB','BC','CC','CD','DD','FR','II']


flag2 = 0

def profinfo(profquery):
	
	for d in dept:
		for year in range(2008,2013):
			for sem in range(1,3):
				address = "http://www.iitb.ac.in/acadpublic/RunningCourses.jsp?deptcd="+str(d)+"&year="+str(year)+"&semester="+str(sem)	
				source=urlopen(address).read()
				source=str(source)
			
				from html.parser import HTMLParser
							
				class MyHTMLParser(HTMLParser):
					y=None
					z=None
					def handle_data(self, data):
						global flag2
						flag=0
						x=str(data)
						#print (x)
						for i in range(len(x)):
							if (x[i]=='\\'):
								flag=1
								break
						if(flag==0)	:
							if(re.search(profquery,x,re.I)):
								flag2 = 1
								print("Name of professor:",x)
								print("Name of course:",self.y) 
								print("Course code:",self.z)
								print("Year:",year)
								print("Semester",sem)
								print("\n")
								self.z=re.sub(' ','',self.z)
								#print(self.z)
								#strng=x+' '+self.z+' '+str(year)+' '+str(sem)
								#print(strng)
								#gradingstatsfunc(strng)
								address1= ('http://asc.iitb.ac.in/academic/Grading/statistics/gradstatforcrse.jsp?year='+str(year)+'&semester='+str(sem)+'&txtcrsecode='+str(self.z)+'&submit=SUBMIT')			
								Multigradestats.allgradestats(address1)		
							self.z=self.y
							self.y=x
							
				parser = MyHTMLParser(strict=False)
				parser.feed(source)

if (flag2==1):
	profinfo(query)
	Slowprofparsing.research(query)
				
if (flag2==0) :
	list =query.split()
	for entry in list:
		profinfo(str(entry))
		Slowprofparsing.research(str(entry))
