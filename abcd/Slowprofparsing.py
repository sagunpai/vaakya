import AEprofparsing
import CSprofparsing
import ENprofparsing
import HSprofparsing
import Geoprofparsing
import IDCprofparsing
import EEprofparsing
import mechprof
import profchem
import profcivil
import MAprofparsing
import metaproffinally


def research(profquery):
	AEprofparsing.profinfo_AE(profquery)
	CSprofparsing.profinfo_CS(profquery)
	ENprofparsing.profinfo_EN(profquery)
	HSprofparsing.profinfo_HS(profquery)
	Geoprofparsing.profinfo_GE(profquery)
	IDCprofparsing.profinfo_ID(profquery)
	EEprofparsing.profinfo_EE(profquery)
	mechprof.profinfo_ME(profquery)
	profcivil.profinfo_CE(profquery)
	MAprofparsing.profinfo_MA(profquery)
	profchem.profinfo_CL(profquery)
	metaproffinally.profinfo_MM(profquery)
