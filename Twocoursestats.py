import urllib
from urllib.request import urlopen
import re
import string
import webbrowser
import numpy as np
import pylab as p
import Multigradestats
import gradecaller
import Singlegradingstats
dept=['AE','CL','CH','CE','CS','GS','EE','HS','MA','SI','ME','MM','PH','EP','GP']
gradelist=['AP','AA','AB','BB','BC','CC','CD','DD','FR','II']


def singlegradingstats2(str1):
	global year
	global sem
	global course
	#print ("Success")
	#print(str1)
	matchcourse=re.search(r'\b\w\w\d\d\d\b|\b\w\w\s\d\d\d\b', str1,re.I)
	matchyear=re.search(r'\b\d\d\d\d\b', str1,flags=0)
	str1=re.sub('Autumn','1',str1,re.I)
	str1=re.sub('Spring','2',str1,re.I)
	matchsem=re.search(r'\b1\b|\b2\b',str1,flags=0)
	
	if matchcourse:
		course=matchcourse.group()
		
	if matchyear:
		year=matchyear.group()
		
		
	if matchsem:
		sem=matchsem.group()
	
	gflag =0
	grade=""
	for g in gradelist:
		matchgrade=re.search(g,str1,re.I)
		if matchgrade:
			grade=matchgrade.group()
			gflag=1
			break
	#if(gflag==0):
		#grade=input("Enter the grade\n").upper()

	address='http://asc.iitb.ac.in/academic/Grading/statistics/gradstatforcrse.jsp?year='+year+'&semester='+sem+'&txtcrsecode='+course+'&submit=SUBMIT'
	#print(address)
	source=gradecaller.call(address)
	match = re.search(grade,source)
	match = re.compile(r'\d+').search(source, match.end())
	#print("Number of "+grade+"s are:"+ match.group())

	gradestats1=Multigradestats.allgradestats(address)

	print("Median grade:"+Multigradestats.median(gradestats1))
	print("Mean grade:"+'%.2f'%(Multigradestats.mean(gradestats1)))
	print(year,sem,course)

def twogradestats(str2):
	course,year,sem=[],[],[]	
	#query=str(input("Enter your query in the form (Course 1 data) (Course 2 data)\n(Data includes course code, semester, year)\n"))
	str2=re.sub('Autumn','1',str2,re.I)
	str2=re.sub('Spring','2',str2,re.I)
	course=re.findall(r"\b\w\w\s\d\d\d\b|\b\w\w\d\d\d\b",str2)
	#course.append(" ").append(" ")
	year=re.findall(r"\b\d\d\d\d\b",str2)
	#year.append(" ").append(" ")
	sem=re.findall(r"\b1\b|\b2\b",str2)
	#sem.append(" ").append(" ")

	
	singlegradingstats2(course[0]+' '+year[0]+' '+sem[0])
	address='http://asc.iitb.ac.in/academic/Grading/statistics/gradstatforcrse.jsp?year='+year[0]+'&semester='+sem[0]+'&txtcrsecode='+course[0]+'&submit=SUBMIT'
	gradestats1=Multigradestats.allgradestats(address)

	singlegradingstats2(course[1]+' '+year[1]+' '+sem[1])
	address='http://asc.iitb.ac.in/academic/Grading/statistics/gradstatforcrse.jsp?year='+year[1]+'&semester='+sem[1]+'&txtcrsecode='+course[1]+'&submit=SUBMIT'
	gradestats2=Multigradestats.allgradestats(address)


	print("Median grade:\n Course 1:\n"+Multigradestats.median(gradestats1)+"\nCourse 2:\n"+Multigradestats.median(gradestats2))
	print("Mean grade:\n Course 1:\n"+'%.2f'%(Multigradestats.mean(gradestats1))+"\nCourse 2:\n"+'%.2f'%(Multigradestats.mean(gradestats2)))

	Multigradestats.graph_2(gradestats1,gradestats2,course[0],course[1],sem[0],sem[1],year[0],year[1])