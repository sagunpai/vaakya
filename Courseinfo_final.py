import urllib
from urllib.request import urlopen
import re
import string
import webbrowser
import numpy as np
import pylab as p
import Multigradestats
import gradecaller
import sys
from sys import argv
query=""
for i in range(1,len(sys.argv)):
	query=query+sys.argv[i]+" "
query=query.strip()

dept=['AE','CL','CH','CE','CS','GS','EE','HS','MA','SI','ME','MM','PH','EP','GP']
gradelist=['AP','AA','AB','BB','BC','CC','CD','DD','FR','II']

def courseinfo(strng):

	#strng=input('query?\n')
	str1=re.search(r'\b\w\w\d\d\d\b|\b\w\w\s\d\d\d\b',strng)
	m=str1.group()
	code=''
	course=''
	str1=re.sub(' ','',m)
	m=str1
	course=m[0]+m[1]
	code=m[2]+m[3]+m[4]
	course=course.upper()
	address='http://www.iitb.ac.in/acadpublic/crsedetail.jsp?ccd='+course+'%20'+code


	source= urlopen(address).read()
	source=str(source)
     

	from html.parser import HTMLParser

	class MyHTMLParser(HTMLParser):
			def handle_data(self, data):
				flag=0
				x=str(data)
				a='\\n'
				
				if (x[0]=='\\'):
					flag=1
					
				if(flag==0)	:
					x=re.sub(r'\\r','',x)
					x=re.sub(r'\\n','',x)
					x=re.sub(r'\\x92','\'',x)
					x=re.sub(r"b'",'',x)
					x=re.sub(r'\\t','',x)
					print(x)

	parser = MyHTMLParser(strict=False)
	parser.feed(source)
	print("Data for the past few times this course has been run :")
	
	
	for year in [2012]:
		for sem in [1,2]:
			address='http://asc.iitb.ac.in/academic/Grading/statistics/gradstatforcrse.jsp?year='+str(year)+'&semester='+str(sem)+'&txtcrsecode='+str(course+code)+'&submit=SUBMIT'
			Multigradestats.allgradestats(address)
code=courseinfo(query)