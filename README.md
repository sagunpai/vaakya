# Vaakya - A Query answering system designed for IITB #

Our aim is to provide comprehensive information about our IITB's academic affairs, all in one place.

Winner of 'Best Coding Project Award' - ITSP 2013. Showcased at Annual Technical Exhibhition 2013, IIT Bombay

### What all does Vaakya answer? ###

* Individual courses: Course details, content, text references, detailed grading statistics (with graphs) and a courses versus course comparision if so desired 
* Topics : We provide a list of courses that teach you a particular topic
* Students : Basic contact details and information about a student, such as his e-mail ID and a link to his Facebook page.
* Professors : Contact details, research interests and a list of recently taught courses, along with their grading stats

### How does Vaakya work? ###

* Each query type is linked to a specific Python program. The user's query is passed on from the HTML webpage to the Python program using PHP.
* The individual programs are designed to search for particular keywords in the text depending on the query, and then crawl through the relevant websites, searching for information.
* The relevant HTML page is parsed and useful information extracted. The required information is then passed back to the Vaakya webpage, which outputs it in the required format, along with relevant tables or graphs

Check out this presentation for more details: https://prezi.com/oslcl-j2wvum/itsp-2013vaakya/

### Feedback ###

Feel free to contact me in case of any issues regarding the same